<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::get();

        return Inertia::render('Admin/Product/index', ['products' => $products]);
    }

    public function createForm()
    {
        $categories = Category::get();

        return Inertia::render('Admin/Product/create', ['categories' => $categories]);
    }

    public function create(Request $request)
    {

        $products = new Product();

        $products->image = $request->file('image')->store('images/products', 'public');
        $products->category_id = $request->category_id;
        $products->name = $request->name;
        $products->description = $request->description;
        $products->qty = $request->qty;
        $products->price = $request->price;
        $products->sale_price = $request->sale_price;
        $products->save();

        return redirect()->route('product');
    }

    public function edit($id)
    {
        $categories = Category::get();

        $products = Product::findOrFail($id);

        return Inertia::render(
            'Admin/Product/edit',
            [
                'products' => $products, 'categories' => $categories
            ]
        );
    }

    public function update(Request $request, $id)
    {
        $products = Product::findOrFail($id);

        if ($request->hasFile('image')) {
            $products->image = $request->file('image')->store('images/products', 'public');
        }
        $products->name = $request->name;
        $products->description = $request->description;
        $products->qty = $request->qty;
        $products->price = $request->price;
        $products->sale_price = $request->sale_price;
        $products->save();

        return redirect()->route('product');
    }

    public function destroy($id)
    {
        $products = Product::findOrFail($id);

        Storage::delete('public/' . $products->image);
        $products->delete();

        return redirect()->route('product');
    }
}
