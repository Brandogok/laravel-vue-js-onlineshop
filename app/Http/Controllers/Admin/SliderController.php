<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;

use function Laravel\Prompts\alert;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::get();

        return Inertia::render('Admin/Slider/index', ['sliders' => $sliders]);
    }

    public function createForm()
    {
        return Inertia::render('Admin/Slider/create');
    }

    public function create(Request $request)
    {
        $slider = new Slider();

        $slider->image = $request->file('image')->store('images/slider', 'public');
        $slider->position = $request->slider_position;
        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->btn_name = $request->btn_name;
        $slider->btn_link = $request->btn_link;
        $slider->status = $request->status;
        $slider->save();

        return redirect()->route('admin');
    }

    public function edit($id)
    {
        $sliders = Slider::findOrFail($id);

        return Inertia::render('Admin/Slider/edit', ['sliders' => $sliders]);
    }

    public function update(Request $request, $id)
    {
        $sliders = Slider::findOrFail($id);

        if ($request->hasFile('image')) {
            $sliders->image = $request->file('image')->store('images/slider', 'public');
        }
        $sliders->position = $request->slider_position;
        $sliders->title = $request->title;
        $sliders->description = $request->description;
        $sliders->btn_name = $request->btn_name;
        $sliders->btn_link = $request->btn_link;
        $sliders->status = $request->status;
        $sliders->save();

        return redirect()->route('slider');
    }

    public function destroy($id)
    {
        $sliders = Slider::findOrFail($id);

        Storage::delete('public/' . $sliders->image);
        $sliders->delete();

        return redirect()->route('slider');
    }
}
