<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $sliders = Slider::get();

        return Inertia::render('Admin/index', ['sliders' => $sliders]);

    }

    public function loginView()
    {

        if (Auth::check()) {
            
            return redirect()->route('admin');
        }
        
        return Inertia::render('Admin/Components/login');
    }
}
