<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::get();

        return Inertia::render('Admin/Categorie/index', ['categories' => $categories]);
    }

    public function createForm()
    {
        return Inertia::render('Admin/Categorie/create');
    }

    public function create(Request $request)
    {
        $category = new Category();

        $category->image = $request->file('image')->store('images/category', 'public');
        $category->category_name = $request->category_name;
        $category->save();

        return redirect()->route('category');
    }
}