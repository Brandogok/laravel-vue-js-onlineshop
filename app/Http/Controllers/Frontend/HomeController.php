<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class HomeController extends Controller
{
    public function index()
    {
        $products = Product::get();

        $sliders = Slider::where('status', true)->get();

        $categories = Category::withCount('products')->get();

        return Inertia::render(
            'Frontend/index',
            ['sliders' => $sliders, 'categories' => $categories, 'products' => $products]
        );
    }

    public function loginView()
    {
        if (Auth::check()) {
            
            return redirect()->route('/');
        }
        
        return Inertia::render('Frontend/Components/login');
    }

    // account
    public function account()
    {
        return Inertia::render('Frontend/Components/user');
    }

    // wishlist

    public function wishlist()
    {
        return Inertia::render('Frontend/Components/wishlist');
    }

    // product_detail

    public function product_detail()
    {
        $products = Product::get();

        return Inertia::render('Frontend/Components/product_detail', ['products' => $products]);
    }


    // shop
        
    public function shop()
    {
        $products = Product::get();

        return Inertia::render('Frontend/Components/shop', ['products' => $products]);
    }
}
