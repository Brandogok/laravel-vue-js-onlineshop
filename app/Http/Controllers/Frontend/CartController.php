<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CartController extends Controller
{

    public function cartView(Request $request)
    {

        $cartItem = session('cart', []);
        $totalPrice = 0;

        foreach ($cartItem as $cart) {

            $totalPrice += $cart['price'] * $cart['qty'];
        }

        return Inertia::render(
            'Frontend/Components/cart',
            [
                'cartItem' => $cartItem,
                'totalPrice' => $totalPrice,
            ]
        );
    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $productId = $id;
        $qty = 1;

        $cartItem = session('cart', []);

        if (isset($cartItem[$id])) {

            // kartta ayni urun varsa adetini arttir
            $cartItem[$id]['qty']++;
        } else {

            $cartItem[$productId] = [
                'id' => $product->id,
                'name' => $product->name,
                'image' => $product->image,
                'qty' => $qty,
                'price' => $product->price,
            ];
        }

        session(['cart' => $cartItem]);

        return redirect()->route('cartView');
    }

    public function deleteCart(Request $request, $id)
    {
        $cartItem = session('cart', []);

        if (isset($cartItem[$id])) {

            unset($cartItem[$id]);

            session(['cart' => $cartItem]);

            return redirect()->route('cartView')->with('message', 'Urun silinmistir');
        }
    }

    public function updateProductQty(Request $request)
    {
        $cartItem = session('cart', []);

        if (isset($cartItem[$request->id])) {

            $cartItem[$request->id]['qty'] = $request->qty;

            session(['cart' => $cartItem]);

            return redirect()->route('cartView');
        }
    }
}
