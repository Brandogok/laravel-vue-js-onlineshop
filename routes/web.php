<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\BrandController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Frontend\HomeController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['admin', config('jetstream.auth_session'), 'verified',])->group(function () {
    Route::get('/admin/dashboard', function () {
        return Inertia::render('Admin/index');
    })->name('dashboard');
});

Route::get('/admin/login', [AdminController::class, 'loginView'])->name('admin.login');

Route::group(['middleware' => ['auth:sanctum', 'admin', config('jetstream.auth_session'), 'verified'], 'prefix' => 'admin'], function () {
    Route::get('/dashboard', [AdminController::class, 'index'])->name('admin');

    Route::get('/slider', [SliderController::class, 'index'])->name('slider');
    Route::get('/slider/createForm', [SliderController::class, 'createForm'])->name('createSlider');
    Route::post('/slider/create', [SliderController::class, 'create'])->name('create');
    Route::get('/slider/{id}/edit', [SliderController::class, 'edit'])->name('edit');
    Route::post('/slider/{id}/update', [SliderController::class, 'update'])->name('update');
    Route::post('/slider/{id}/delete', [SliderController::class, 'destroy'])->name('delete');

    Route::get('/brand', [BrandController::class, 'index'])->name('brand');

    Route::get('/category', [CategoryController::class, 'index'])->name('category');
    Route::get('/category/createForm', [CategoryController::class, 'createForm'])->name('createCategorie');
    Route::post('/category/create', [CategoryController::class, 'create'])->name('create');
    Route::get('/category/{id}/edit', [CategoryController::class, 'edit'])->name('edit');
    Route::post('/category/{id}/update', [CategoryController::class, 'update'])->name('update');
    Route::post('/category/{id}/delete', [CategoryController::class, 'destroy'])->name('delete');

    Route::get('/product', [ProductController::class, 'index'])->name('product');
    Route::get('/product/createForm', [ProductController::class, 'createForm'])->name('createForm');
    Route::post('/product/create', [ProductController::class, 'create'])->name('create');
    Route::get('/product/{id}/edit', [ProductController::class, 'edit'])->name('edit');
    Route::post('/product/{id}/update', [ProductController::class, 'update'])->name('update');
    Route::post('/product/{id}/delete', [ProductController::class, 'destroy'])->name('delete');
});

Route::get('/', [HomeController::class, 'index'])->name('/');
Route::get('/login', [HomeController::class, 'loginView'])->name('login');

Route::get('/shop', [HomeController::class, 'shop'])->name('shop');
Route::get('/product-detail', [HomeController::class, 'product_detail'])->name('product_detail');

Route::middleware('auth')->group(function () {

    Route::get('/cart', [CartController::class, 'cartView'])->name('cartView');
    Route::get('/add-to-cart/{id}', [CartController::class, 'addToCart'])->name('addToCart');
    Route::get('/delete-from-cart/{id}', [CartController::class, 'deleteCart'])->name('deleteCart');
    Route::post('/update-product-qty', [CartController::class, 'updateProductQty'])->name('updateProductQty');
    Route::get('/wishlist', [HomeController::class, 'wishlist'])->name('wishlist');
    Route::get('/account', [HomeController::class, 'account'])->name('account');
});

// Route::get('/', function () {
//     return Inertia::render('Welcome', [
//         'canLogin' => Route::has('login'),
//         'canRegister' => Route::has('register'),
//         'laravelVersion' => Application::VERSION,
//         'phpVersion' => PHP_VERSION,
//     ]);
// });

// Route::middleware([
//     'auth:sanctum',
//     config('jetstream.auth_session'),
//     'verified',
// ])->group(function () {
//     Route::get('/dashboard', function () {
//         return Inertia::render('Dashboard');
//     })->name('dashboard');
// });